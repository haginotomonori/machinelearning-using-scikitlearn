# -*- coding: utf-8 -*-
# https://dev.classmethod.jp/machine-learning/introduction-scikit-learn/

from sklearn.datasets import load_digits

digits = load_digits(10)
# print(digits.data.shape)

from sklearn.linear_model import LogisticRegression
train_X = digits.data[:1500]
train_Y = digits.target[:1500]

test_X = digits.data[1500:]
test_Y = digits.target[1500:]

lr = LogisticRegression()
lr.fit(train_X, train_Y)

pred = lr.predict(test_X)
# print(pred)

from sklearn.metrics import confusion_matrix
matrix = confusion_matrix(test_Y, pred, labels=digits.target_names)
print(matrix)