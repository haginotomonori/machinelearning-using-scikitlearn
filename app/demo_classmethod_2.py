# -*- coding: utf-8 -*-
# https://dev.classmethod.jp/machine-learning/introduction-scikit-learn/
# より実践的に

import os
import numpy as np
 
from sklearn.datasets import load_digits
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.externals import joblib


def learning(train_X, train_y):
    # Pipelineを作成
    # データの正規化とSVMを定義
    pipeline = Pipeline([
        ('standard_scaler', StandardScaler()),
        ('svm', SVC())])

    # パラメータの探索範囲を指定
    # Grid Search用のパラメータは本来であればもっと細かくやったほうがいい
    params = {
        'svm__C': np.logspace(0, 2, 5),
        'svm__gamma': np.logspace(-3, 0, 5)
    }

    # Grid Searchを行う
    clf = GridSearchCV(pipeline, params)
    clf.fit(train_X, train_y)

    # モデルの保存
    # APIなどで利用する際はjoblib.loadで保存したモデルを読み込んで、入力されたデータに対してpredictを行えば良い
    print('put clf file. ' + path)
    joblib.dump(clf, path)

    return clf

 
digits = load_digits(10)
 
train_X = digits.data[:1500]
train_y = digits.target[:1500]
 
test_X = digits.data[1500:]
test_y = digits.target[1500:]

path = os.path.dirname(__file__) + '/var/clf.pkl'
if not os.path.exists(path):
    clf = learning(train_X, train_y)
else:
    print('clf file found.')
    clf = joblib.load(path)

pred = clf.predict(test_X)
 
# 結果のレポーティング
print('########## reporitng ##########')
print(classification_report(test_y, pred))
print(confusion_matrix(test_y, pred))

