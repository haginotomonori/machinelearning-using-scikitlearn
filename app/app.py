# -*- coding: utf-8 -*-
import random
from flask import Flask, render_template, request, jsonify
from flask_bootstrap import Bootstrap
from flask_restplus import Api, Resource, fields, Namespace


app = Flask(__name__)
app.secret_key = 'f8e98gvja9201vapoi3r0918vja'
app.config['JSON_AS_ASCII'] = False
Bootstrap(app)
api = Api(app)


@app.route("/form")
def hello():
    return render_template('index.html', message='hello world')


# todo apiを別フォルダにしたい
post_spec = api.model('prot0 post', {
    'text': fields.String(description="text", example="ある日の暮方の事である。一人の下人げにんが、羅生門の下で雨やみを待っていた。")
})


prot1_post_spec = api.model('prot1 post', {
    'url': fields.String(description="text", example="https://en.wikipedia.org/wiki/Automatic_summarization")
})


@api.route('/api/proto0', methods=['POST'])
class Proto0(Resource):
    @api.expect(post_spec)
    def post(self):
        try:
            if request.form and request.form['text']:
                text = request.form['text']
            elif request.json and request.json['text']:
                text = request.json['text']
            else:
                text = 'dummy text'

            text_list = []
            for x in range(5):
                to = random.randint(1, len(text))
                text_list.append(text[0:to])

            result = {
                "status": 200,
                "data": text_list
            }
            return jsonify(result)
        except Exception as e:
            return str(e)


from sumy.parsers.html import HtmlParser
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer as Summarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words


LANGUAGE = "english"
SENTENCES_COUNT = 10


@api.route('/api/proto1', methods=['POST'])
class SumyDemo(Resource):
    @api.expect(prot1_post_spec)
    def post(self):
        if request.form and request.form['url']:
            url = request.form['url']
        elif request.json and request.json['url']:
            url = request.json['url']
        else:
            url = "https://en.wikipedia.org/wiki/Automatic_summarization"

        print(url)

        parser = HtmlParser.from_url(url, Tokenizer(LANGUAGE))
        stemmer = Stemmer(LANGUAGE)

        summarizer = Summarizer(stemmer)
        summarizer.stop_words = get_stop_words(LANGUAGE)

        text = ''
        for sentence in summarizer(parser.document, SENTENCES_COUNT):
            # print(sentence)
            text = text + str(sentence)

        # print(text_list)

        result = {
            "status": 200,
            "data": [text]
        }

        return jsonify(result)


if __name__ == "__main__":
    app.run()
