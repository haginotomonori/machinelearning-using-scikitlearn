
  
# 概要 機械学習ハンズオン    

dockerでscikit-learnをはじめとする機械学習ライブラリを動かす環境を作る
flaskでAPIを作ってherokuにデプロイ
url: https://machin-learning.herokuapp.com/
    
# link 
* [優先度学習による推薦文からの見出し抽出](http://www.orsj.or.jp/archive2/or62-11/or62_11_731.pdf)
* [自然言語処理の分類タスクを試行錯誤する](https://qiita.com/akichim21/items/b72a51975bfb6b9f157e)
    
# commands    
 コンテナ起動と接続、実行    
```bash docker-compose up -d --build docker exec -it machin-learning_app /bin/bash python app/demo_classmethod.py ```    
 localから叩く    
```bash docker-compose run app python app/demo_classmethod.py ```    
 ## heroku command  
  
herokuコンテナに接続して行う  
    
deploy    
```bash git subtree push --prefix app heroku master ```    
 herokuサーバー上でコマンド実行    
```bash heroku run python --version ```    
 herokuに接続    
```bash heroku run bash ```    
 log確認    
```bash heroku logs --tail  
```  
  
python -c "import nltk; nltk.download('punkt') が出るのでnltk.txtを追加してみたがダメだった  
  
  
## 履歴  
* scikit-learnが動くdocker環境を作成
	* [scikit-learn](https://scikit-learn.org/stable/documentation.html#) 
	* [scikit-learn「アルゴリズム・チートシート」の全手法を実装・解説](https://qiita.com/sugulu/items/e3fc39f2e552f2355209)    
* scikit-learnハンズオンを試す
	* [いまさら聞けない？scikit-learnのキホン - DevelopsersIO](https://dev.classmethod.jp/machine-learning/introduction-scikit-learn/)   
* flaskを追加  
	* [flask pythonの軽量フレームワーク](http://flask.pocoo.org/)
* formからpostしたテキストを切り出して返すAPIを追加  
* herokuにデプロイしてglobalからAPIを実行できるようにした
	* [Deploy Git subdirectory to Heroku](https://medium.com/@shalandy/deploy-git-subdirectory-to-heroku-ea05e95fce1f)
* Flask-RESTPlusを追加
	* [https://qiita.com/sky_jokerxx/items/17481ffc34b52875528b](https://qiita.com/sky_jokerxx/items/17481ffc34b52875528b)
	* [flask-restplus doc](https://flask-restplus.readthedocs.io/en/stable/)
* sumyによる要約機能を追加
	* [sumy テキスト要約モジュール](https://github.com/miso-belica/sumy)
	* [LexRankで日本語の記事を要約する](https://ohke.hateblo.jp/entry/2018/11/17/230000)
	* [sumyを使って青空文庫を要約してみる](https://qiita.com/hideki/items/5e9892094ae786d2ad6c)